using UnityEngine;
using Zenject;

namespace Balls.SpawnPoint
{
    public class BigBallSpawnPointFacade : MonoBehaviour
    {
        public Vector3 Position { get; set; }
        public bool IsOccupied { get; set; }

        public class Factory : PlaceholderFactory<BigBallSpawnPointFacade> { }
    }
}
