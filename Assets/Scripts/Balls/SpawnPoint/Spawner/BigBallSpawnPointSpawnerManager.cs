using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Balls.SpawnPoint.Spawner
{
    public class BigBallSpawnPointSpawnerManager : IInitializable
    {
        private readonly BigBallSpawnPointFacade.Factory _bigBallSpawnPointControllerFactory;
        private readonly Settings _settings;
        private readonly List<BigBallSpawnPointFacade> _bigBallSpawnPointFacades = new List<BigBallSpawnPointFacade>();

        public BigBallSpawnPointSpawnerManager(
            BigBallSpawnPointFacade.Factory bigBallSpawnPointControllerFactory,
            Settings settings)
        {
            _bigBallSpawnPointControllerFactory = bigBallSpawnPointControllerFactory;
            _settings = settings;
        }

        public void Initialize()
        {
            // todo Refactor this and make this system more flexible for different screen resolutions and easier to design

            var y = _settings.startYPos;
            SpawnThreeSpawnPoints(y);
            
            y -= _settings.rowIntervals;
            SpawnTwoSpawnPoints(y);
            
            y -= _settings.rowIntervals;
            SpawnThreeSpawnPoints(y);
            
            y -= _settings.rowIntervals;
            SpawnTwoSpawnPoints(y);
        }

        private void SpawnThreeSpawnPoints(float posY)
        {
            const float x = -3;
            for (var i = 0; i < 3; i++)
            {
                CreatePoint(posY, x, i);
            }
        }

        private void CreatePoint(float posY, float x, int i)
        {
            var spawnPoint = _bigBallSpawnPointControllerFactory.Create();

            SetSpawnPointPosition(posY, x, i, spawnPoint);

            _bigBallSpawnPointFacades.Add(spawnPoint);
        }

        private static void SetSpawnPointPosition(float posY, float x, int i, BigBallSpawnPointFacade spawnPoint)
        {
            spawnPoint.Position = new Vector3(x + i * 3, posY);
        }

        private void SpawnTwoSpawnPoints(float posY)
        {
            const float x = -1.5f;
            for (var i = 0; i < 2; i++)
            {
                CreatePoint(posY, x, i);
            }
        }

        public int GetAllAvailableSpawnPointsCount()
        {
            // Decrease list by one to always leave one free spawn point in case user buy new ball. 
            return _bigBallSpawnPointFacades.Count - 1;
        }

        public BigBallSpawnPointFacade GetRandomAvailableSpawnPoint()
        {
            BigBallSpawnPointFacade spawnPoint;
            do
            {
                var spawnIndex = Random.Range(0, _bigBallSpawnPointFacades.Count);
                spawnPoint = _bigBallSpawnPointFacades[spawnIndex];
            } while (spawnPoint.IsOccupied);

            return spawnPoint;
        }

        [Serializable]
        public class Settings
        {
            public int startYPos = 5;
            public int rowIntervals = 3;
        }
    }
}