using System;
using System.Collections.Generic;
using Balls.BigBall.ScriptableObject;
using Balls.SpawnPoint.Spawner;
using Zenject;

namespace Balls.BigBall
{
    public class BigBallSpawnerManager : IInitializable
    {
        private readonly BigBallSpawnPointSpawnerManager _bigBallSpawnPointSpawnerManager;
        private readonly BigBallFacade.Factory _bigBallControllerFactory;
        private readonly Settings _settings;

        public BigBallSpawnerManager(
            BigBallSpawnPointSpawnerManager bigBallSpawnPointSpawnerManager,
            BigBallFacade.Factory bigBallControllerFactory,
            Settings settings)
        {
            _bigBallSpawnPointSpawnerManager = bigBallSpawnPointSpawnerManager;
            _bigBallControllerFactory = bigBallControllerFactory;
            _settings = settings;
        }

        public void Initialize()
        {
            var availableBallsToSpawn = GetAvailableBallsToSpawn();
            
            var availableSpawnPointsCount = _bigBallSpawnPointSpawnerManager.GetAllAvailableSpawnPointsCount() /
                                            availableBallsToSpawn.Count;
            
            foreach (var bigBallSettingsSO in availableBallsToSpawn)
            {
                for (int j = 0; j < availableSpawnPointsCount; j++)
                {
                    var spawnPoint = _bigBallSpawnPointSpawnerManager.GetRandomAvailableSpawnPoint();
                    
                    _bigBallControllerFactory.Create(bigBallSettingsSO, spawnPoint);
                }
            }
        }

        public List<BigBallSettingsSO> GetAvailableBallsToSpawn()
        {
            var typeOfBallsToSpawn = _settings.bigBallSettingsManagerSO.GetAvailableBigBallsToSpawn();
            return typeOfBallsToSpawn;
        }

        [Serializable]
        public class Settings
        {
            public BigBallSettingsManagerSO bigBallSettingsManagerSO;
        }
    }
}
