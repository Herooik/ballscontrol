using Balls.BigBall.ScriptableObject;
using Balls.SpawnPoint;
using UI.BigBall;
using UnityEngine;
using Zenject;

namespace Balls.BigBall
{
    public class BigBallFacade : MonoBehaviour,IPoolable<BigBallSettingsSO, BigBallSpawnPointFacade, IMemoryPool>
    {
        public BigBallSettingsSO CurrentBigBallSettingsSO { get; private set; }

        private IMemoryPool _memoryPool;
        private UIBigBallHandler _uiBigBallHandler;
        private BigBallSpawnPointFacade _currentSpawnPoint;
        private BigBallPointsController _bigBallPointsController;
        private BigBallRegistry _bigBallRegistry;

        [Inject]
        public void Construct(
            UIBigBallHandler uiBigBallHandler,
            BigBallPointsController bigBallPointsController,
            BigBallRegistry bigBallRegistry)
        {
            _uiBigBallHandler = uiBigBallHandler;
            _bigBallPointsController = bigBallPointsController;
            _bigBallRegistry = bigBallRegistry;
        }

        public Vector2 CurrentPosition
        {
            get => transform.position;
            private set => transform.position = value;
        }

        public void OnDespawned()
        {
            _bigBallRegistry.RemoveBall(this);

            SetSpawnPointOccupied(false);

            CurrentBigBallSettingsSO = null;
            _currentSpawnPoint = null;
            _memoryPool = null;
        }

        public void OnSpawned(BigBallSettingsSO bigBallSettingsSO, BigBallSpawnPointFacade spawnPoint, IMemoryPool pool)
        {
            CurrentBigBallSettingsSO = bigBallSettingsSO;
            _currentSpawnPoint = spawnPoint;
            _memoryPool = pool;

            SetSpawnPointOccupied(true);
            
            _uiBigBallHandler.SetOnSpawn(CurrentBigBallSettingsSO);
            
            SetPosition();

            _bigBallPointsController.SetStartingPoints(CurrentBigBallSettingsSO);
            
            _bigBallRegistry.AddBall(this);
        }

        private void SetSpawnPointOccupied(bool occupied)
        {
            _currentSpawnPoint.IsOccupied = occupied;
        }

        private void SetPosition()
        {
            CurrentPosition = _currentSpawnPoint.Position;
        }

        public void Dispose()
        {
            _memoryPool.Despawn(this);
        }

        public void OnSmallBallEnterCollider()
        {
            _bigBallPointsController.ReducePoint(CurrentBigBallSettingsSO.GetBallPoint());
        }

        public class Factory : PlaceholderFactory<BigBallSettingsSO, BigBallSpawnPointFacade, BigBallFacade> { }
    }
}
