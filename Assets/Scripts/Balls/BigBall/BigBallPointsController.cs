using Balls.BigBall.ScriptableObject;
using Stats;
using UI.BigBall;
using UnityEngine;

namespace Balls.BigBall
{
    public class BigBallPointsController
    {
        private readonly BigBallDestroyHandler _bigBallDestroyHandler;
        private readonly GamePointsManager _gamePointsManager;
        private readonly UIBigBallHandler _uiBigBallHandler;
        private int _startingPoints;
        private int _currentPoints;

        public BigBallPointsController
            (BigBallDestroyHandler bigBallDestroyHandler,
                GamePointsManager gamePointsManager,
                UIBigBallHandler uiBigBallHandler)
        {
            _bigBallDestroyHandler = bigBallDestroyHandler;
            _gamePointsManager = gamePointsManager;
            _uiBigBallHandler = uiBigBallHandler;
        }
        
        public void ReducePoint(int i)
        {
            _currentPoints -= i;
            
            if (_currentPoints <= 0)
            {
                _gamePointsManager.AddPoint(_startingPoints);
                
                _bigBallDestroyHandler.DestroyBall();
            }
            
            _uiBigBallHandler.SetCurrentPointsText(_currentPoints);
        }

        public void SetStartingPoints(BigBallSettingsSO currentBigBallSettingsSO)
        {
            _startingPoints = _gamePointsManager.GetCurrentLevel() * currentBigBallSettingsSO.SpawnerWage;

            _currentPoints = _startingPoints;
            
            _uiBigBallHandler.SetCurrentPointsText(_currentPoints);
        }
    }
}