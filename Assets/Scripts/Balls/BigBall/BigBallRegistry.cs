﻿using System.Collections.Generic;
using System.Linq;
using Balls.BigBall.ScriptableObject;

namespace Balls.BigBall
{
    public class BigBallRegistry
    {
        private readonly List<BigBallFacade> _bigBallFacades = new List<BigBallFacade>();
        
        public void AddBall(BigBallFacade bigBallFacade)
        {
            _bigBallFacades.Add(bigBallFacade);
        }

        public void RemoveBall(BigBallFacade smallBallFacade)
        {
            _bigBallFacades.Remove(smallBallFacade);
        }

        public bool CheckIfBigBallExist(BigBallSettingsSO currentBigBallSettingsSO)
        {
            return _bigBallFacades.Any(bigBallFacade => bigBallFacade.CurrentBigBallSettingsSO == currentBigBallSettingsSO);
        }

        public List<BigBallSettingsSO> GetCurrentSpawnedBigBallsType()
        {
            var list = new List<BigBallSettingsSO>();
            foreach (var bigBallFacade in _bigBallFacades)
            {
                var currentBigBallSettingsSO = bigBallFacade.CurrentBigBallSettingsSO;
                if (!list.Contains(currentBigBallSettingsSO))
                {
                    list.Add(currentBigBallSettingsSO);
                }
            }

            return list;
        }
    }
}