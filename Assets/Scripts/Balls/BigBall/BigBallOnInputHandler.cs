﻿using Balls.SmallBall;
using Installers;
using Misc;
using UnityEngine;
using Zenject;

namespace Balls.BigBall
{
    public class BigBallOnInputHandler : MonoBehaviour, IInteractableObject
    {
        private BigBallFacade _bigBallFacade;
        private SignalBus _signalBus;

        [Inject]
        public void Construct(
            BigBallFacade bigBallFacade,
            SignalBus signalBus)
        {
            _bigBallFacade = bigBallFacade;
            _signalBus = signalBus;
        }

        public void OnBeganInput()
        {
        }

        public void OnStationaryInput()
        {
            MoveAllSmallBalls();
        }

        public void OnEndedInput()
        {
        }

        private void MoveAllSmallBalls()
        {
            _signalBus.Fire(new GameEvents.MoveAllSmallBallsToBigBallSignal(_bigBallFacade));
        }
    }
}