﻿using Installers;
using Misc;
using Zenject;

namespace Balls.BigBall
{
    public class BigBallDestroyHandler
    {
        private readonly BigBallFacade _bigBallFacade;
        private readonly SignalBus _signalBus;

        public BigBallDestroyHandler(
            BigBallFacade bigBallFacade,
            SignalBus signalBus)
        {
            _bigBallFacade = bigBallFacade;
            _signalBus = signalBus;
        }

        public void DestroyBall()
        {
            // todo handle particles
            // todo handle audio

            _signalBus.Fire(new GameEvents.BigBallDestroyedSignalTest(_bigBallFacade));
            
            _bigBallFacade.Dispose();
        }
    }
}