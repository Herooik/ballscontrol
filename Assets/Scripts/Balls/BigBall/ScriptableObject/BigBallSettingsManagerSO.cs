﻿using System.Collections.Generic;
using UnityEngine;

namespace Balls.BigBall.ScriptableObject
{
    [CreateAssetMenu(fileName = "BigBallManager", menuName = "BallsControl/Balls/New Big Ball Manager Settings")]
    public class BigBallSettingsManagerSO : UnityEngine.ScriptableObject
    {
        [SerializeField] private List<BigBallSettingsSO> bigBallSettingsList;

        public List<BigBallSettingsSO> GetAvailableBigBallsToSpawn()
        {
            var tempList = new List<BigBallSettingsSO>();
            
            foreach (var bigBallSettingsSO in bigBallSettingsList)
            {
                if (bigBallSettingsSO.IsUnlocked())
                {
                    tempList.Add(bigBallSettingsSO);
                }
            }

            return tempList;
        }

        public List<BigBallSettingsSO> GetAllBalls()
        {
            return bigBallSettingsList;
        }
    }
}