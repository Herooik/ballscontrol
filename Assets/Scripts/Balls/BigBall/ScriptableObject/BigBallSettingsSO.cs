﻿using UnityEngine;

namespace Balls.BigBall.ScriptableObject
{
    [CreateAssetMenu(fileName = "NewBigBall", menuName = "BallsControl/Balls/New Big Ball Settings")]
    public class BigBallSettingsSO : UnityEngine.ScriptableObject
    {
        public int CurrentUpgradeLevel = 1;

        [SerializeField] private string ballName;
        [SerializeField] private Color ballColor;
        [SerializeField] private int startUnlockPrice;
        [SerializeField] private int spawnerWage;
        [SerializeField, Range(0,1000)] private int smallBallPointMultiplier = 1;

        private const float NextUpgradeCostMultiplier = 1.7f;

        public bool IsUnlocked()
        {
            return CurrentUpgradeLevel >= 1;
        }

        public string GetBallName()
        {
            return ballName;
        }

        public Color GetColor()
        {
            return ballColor;
        }

        public int GetUnlockPrice()
        {
            float value = startUnlockPrice;
            
            for (var i = 0; i < CurrentUpgradeLevel; i++)
            {
                value *= NextUpgradeCostMultiplier;
            }

            return (int)value;
        }

        public int GetBallPoint()
        {
            return smallBallPointMultiplier * CurrentUpgradeLevel;
        }

        public int SpawnerWage => spawnerWage;
    }
}