namespace Balls
{
    public interface IInteractableObject
    {
        void OnBeganInput();
        void OnStationaryInput();
        void OnEndedInput();
    }
}