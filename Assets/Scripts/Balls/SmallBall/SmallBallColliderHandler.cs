﻿using System;
using Balls.BigBall;
using Installers;
using Misc;
using UnityEngine;
using Zenject;

namespace Balls.SmallBall
{
    public class SmallBallColliderHandler : MonoBehaviour
    {
        [SerializeField] private Collider2D collider2d;

        private BigBallFacade _targetBigBall;
        private SmallBallVelocityController _smallBallVelocityController;
        private SmallBallFacade _smallBallFacade;
        private SignalBus _signalBus;

        [Inject]
        public void Construct(
            SmallBallVelocityController smallBallVelocityController,
            SmallBallFacade smallBallFacade,
            SignalBus signalBus)
        {
            _smallBallVelocityController = smallBallVelocityController;
            _smallBallFacade = smallBallFacade;
            _signalBus = signalBus;
        }

        private void OnEnable()
        {
            _signalBus.Subscribe<GameEvents.BigBallDestroyedSignalTest>(OnBigBallDestroyed);
            _signalBus.Subscribe<GameEvents.MoveAllSmallBallsToBigBallSignal>(OnMoveToBigBall);
        }

        private void OnDisable()
        {
            _signalBus.Unsubscribe<GameEvents.BigBallDestroyedSignalTest>(OnBigBallDestroyed);
            _signalBus.Unsubscribe<GameEvents.MoveAllSmallBallsToBigBallSignal>(OnMoveToBigBall);
        }

        private void OnBigBallDestroyed(GameEvents.BigBallDestroyedSignalTest obj)
        {
            if(obj.BigBallFacade == _targetBigBall) 
                SetToDefault();
        }

        private void SetToDefault()
        {
            SetTargetBigBall(null);
            
            SetColliderTrigger(false);
        }

        private void SetTargetBigBall(BigBallFacade target)
        {
            _targetBigBall = target;
        }

        private void SetColliderTrigger(bool state)
        {
            collider2d.isTrigger = state;
        }

        private void OnMoveToBigBall(GameEvents.MoveAllSmallBallsToBigBallSignal obj)
        {
            SetTargetBigBall(obj.BigBallFacade);
        }

        public void SetColliderOnSpawn()
        {
            SetColliderTrigger(true);
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject == _targetBigBall?.gameObject)
            {
                SetColliderTrigger(true);
                return;
            }

            BounceBall(collision);
        }

        private void BounceBall(Collision2D collision)
        {
            var speed = _smallBallVelocityController.LastVelocity.magnitude;
            var direction = Vector3.Reflect(_smallBallVelocityController.LastVelocity.normalized,
                collision.contacts[0].normal);

            _smallBallVelocityController.SetCurrentVelocity(direction * speed);
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            var bigBallFacade = other.gameObject.GetComponent<BigBallFacade>();
            if (_targetBigBall && bigBallFacade == _targetBigBall)
            {
                _targetBigBall.OnSmallBallEnterCollider();

                _smallBallFacade.DestroyBall();

                SetTargetBigBall(null);
            }
        }

        public void OnTriggerExit2D(Collider2D other)
        {
            if(other.GetComponent<GameplayBoundariesController>())
                SetColliderTrigger(false);
        }
    }
}