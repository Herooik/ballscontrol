using System;
using System.Threading.Tasks;
using Balls.BigBall;
using Balls.BigBall.ScriptableObject;
using Camera;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Balls.SmallBall.Spawner
{
    public class SmallBallSpawnerManager : IInitializable
    {
        private readonly Settings _settings;
        private readonly SmallBallFacade.Factory _smallBallFactory;
        private readonly BigBallSpawnerManager _bigBallSpawnerManager;
        private readonly CameraController _cameraController;
        private readonly BigBallRegistry _bigBallRegistry;

        public SmallBallSpawnerManager(
            Settings settings,
            SmallBallFacade.Factory smallBallFactory,
            BigBallSpawnerManager bigBallSpawnerManager,
            CameraController cameraController,
            BigBallRegistry bigBallRegistry)
        {
            _settings = settings;
            _smallBallFactory = smallBallFactory;
            _bigBallSpawnerManager = bigBallSpawnerManager;
            _cameraController = cameraController;
            _bigBallRegistry = bigBallRegistry;
        }

        public void Initialize()
        {
            SpawnBallsAsyncAtStart();
        }

        private async void SpawnBallsAsyncAtStart()
        {
            await UniTask.Delay(_settings.waitTimeAtStartInMilliSeconds);

            var typeOfBallsToSpawn = _bigBallSpawnerManager.GetAvailableBallsToSpawn();

            var ballsToSpawn = _settings.maxSpawns / typeOfBallsToSpawn.Count;
            
            foreach (var bigBallSettingsSO in typeOfBallsToSpawn)
            {
                for (int j = 0; j < ballsToSpawn; j++)
                {
                    await StartInterval();

                    SpawnSmallBall(bigBallSettingsSO);
                }
            }
        }

        private async UniTask StartInterval()
        {
            var intervalInMilliSeconds = _settings.spawnIntervalInMilliSeconds;

            await UniTask.Delay(intervalInMilliSeconds);
        }

        private void SpawnSmallBall(BigBallSettingsSO bigBallSettingsSO)
        {
            var smallBallController = _smallBallFactory.Create(bigBallSettingsSO);

            smallBallController.CurrentPosition = SetNewPosition();
            
            smallBallController.SetVelocityOnSpawn();
        }

        private Vector2 SetNewPosition()
        {
            var leftX = _cameraController.GetUpperLeftCorner().x - 0.5f;
            var rightX = _cameraController.GetUpperRightCorner().x + 0.5f;

            var posX = Random.value >= 0.5f ? leftX : rightX;
            var posY = Random.Range(_settings.spawnPosMinY, _settings.spawnPosMaxY);
        
            return new Vector2(posX, posY);
        }

        public async void SpawnOnDestroyAllSmallBallsAsync(BigBallSettingsSO currentBigBallSettingsSO)
        {
            if (!_bigBallRegistry.CheckIfBigBallExist(currentBigBallSettingsSO)) return;
            
            var typeOfBallsToSpawn = _bigBallRegistry.GetCurrentSpawnedBigBallsType();

            var ballsToSpawn = _settings.maxSpawns / typeOfBallsToSpawn.Count;

            for (var j = 0; j < ballsToSpawn; j++)
            {
                await StartInterval();

                SpawnSmallBall(currentBigBallSettingsSO);
            }
        }

        [Serializable]
        public class Settings
        {
            public int maxSpawns;
            public float spawnPosMaxY = 7f;
            public float spawnPosMinY = -6f;
            [Range(0, 500)] public int spawnIntervalInMilliSeconds;
            [Range(0, 500)] public int waitTimeAtStartInMilliSeconds = 500;
        }
    }
}