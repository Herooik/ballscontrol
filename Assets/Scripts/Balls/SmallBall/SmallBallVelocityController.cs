using System;
using UnityEngine;
using Zenject;

namespace Balls.SmallBall
{
    public class SmallBallVelocityController : IFixedTickable
    {
        private readonly Settings _settings;
        private readonly SmallBallRigidbodyHandler _smallBallRigidbodyHandler;
        private readonly SmallBallFacade _smallBallFacade;

        private SmallBallVelocityController(
            Settings settings,
            SmallBallRigidbodyHandler smallBallRigidbodyHandler,
            SmallBallFacade smallBallFacade)
        {
            _settings = settings;
            _smallBallRigidbodyHandler = smallBallRigidbodyHandler;
            _smallBallFacade = smallBallFacade;
        }

        public Vector2 LastVelocity { get; private set; }

        public Vector2 CurrentVelocity
        {
            get => _smallBallRigidbodyHandler.Rigidbody2d.velocity;
            set => _smallBallRigidbodyHandler.Rigidbody2d.velocity = value;
        }

        public void FixedTick()
        {
            SetCurrentVelocity(CurrentVelocity.normalized * _settings.speed);

            LastVelocity = CurrentVelocity;
        }

        public void SetCurrentVelocity(Vector2 velocity)
        {
            CurrentVelocity = velocity;
        }

        public void SetVelocityAtStart()
        {
            var forceVector = _smallBallFacade.CurrentPosition.x > 0 ? Vector2.left : Vector2.right;

            SetCurrentVelocity(forceVector * _settings.speed);
        }

        [Serializable]
        public class Settings
        {
            public float speed = 4f;
        }
    }
}