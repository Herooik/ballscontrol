﻿namespace Balls.SmallBall
{
    public class SmallBallDestroyHandler
    {
        private readonly SmallBallFacade _smallBallFacade;
        
        public SmallBallDestroyHandler(
            SmallBallFacade smallBallFacade)
        {
            _smallBallFacade = smallBallFacade;
        }
        
        public void DestroyBall()
        {
            // TODO handle audio
            // todo handle particles 
            
            _smallBallFacade.Dispose();
        }
    }
}