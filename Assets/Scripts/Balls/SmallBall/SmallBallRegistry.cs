﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Balls.BigBall.ScriptableObject;
using Balls.SmallBall.Spawner;
using UnityEngine;

namespace Balls.SmallBall
{
    public class SmallBallRegistry
    {
        private readonly List<SmallBallFacade> _smallBalls = new List<SmallBallFacade>();
        private readonly SmallBallSpawnerManager _smallBallSpawnerManager;

        public SmallBallRegistry(SmallBallSpawnerManager smallBallSpawnerManager)
        {
            _smallBallSpawnerManager = smallBallSpawnerManager;
        }
        
        public void AddBall(SmallBallFacade smallBallFacade)
        {
            _smallBalls.Add(smallBallFacade);
        }

        public void RemoveBall(SmallBallFacade smallBallFacade)
        {
            _smallBalls.Remove(smallBallFacade);
            
            if (GetAllBallsByType(smallBallFacade.CurrentBigBallSettingsSO).Count == 0)
            {
                _smallBallSpawnerManager.SpawnOnDestroyAllSmallBallsAsync(smallBallFacade.CurrentBigBallSettingsSO);
            }
        }

        private List<SmallBallFacade> GetAllBallsByType(BigBallSettingsSO targetBigBallSettings)
        {
            return _smallBalls.Where(smallBallFacade => smallBallFacade.CurrentBigBallSettingsSO == targetBigBallSettings)
                .ToList();
        }
    }
}