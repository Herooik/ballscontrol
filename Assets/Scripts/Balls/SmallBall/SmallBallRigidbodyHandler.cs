﻿using UnityEngine;

namespace Balls.SmallBall
{
    public class SmallBallRigidbodyHandler : MonoBehaviour
    {
        public Rigidbody2D Rigidbody2d => rigidbody2d;
        public Vector2 CurrentPosition => rigidbody2d.position;

        [SerializeField] private Rigidbody2D rigidbody2d;
    }
}