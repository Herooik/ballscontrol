﻿using System;
using Balls.BigBall;
using Installers;
using Misc;
using UnityEngine;
using Zenject;

namespace Balls.SmallBall
{
    public class SmallBallAnimationHandler : IFixedTickable, IInitializable, IDisposable
    {
        private readonly SmallBallRigidbodyHandler _smallBallRigidbodyHandler;
        private readonly Settings _settings;
        private readonly SignalBus _signalBus;
        private BigBallFacade _target;

        private SmallBallAnimationHandler(
            SmallBallRigidbodyHandler smallBallRigidbodyHandler,
            Settings settings,
            SignalBus signalBus)
        {
            _smallBallRigidbodyHandler = smallBallRigidbodyHandler;
            _settings = settings;
            _signalBus = signalBus;
        }
        
        public void Initialize()
        {
            _signalBus.Subscribe<GameEvents.BigBallDestroyedSignalTest>(OnBigBallDestroy);
            _signalBus.Subscribe<GameEvents.MoveAllSmallBallsToBigBallSignal>(OnMoveToBigBall);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<GameEvents.BigBallDestroyedSignalTest>(OnBigBallDestroy);
            _signalBus.Unsubscribe<GameEvents.MoveAllSmallBallsToBigBallSignal>(OnMoveToBigBall);
        }

        private void OnBigBallDestroy(GameEvents.BigBallDestroyedSignalTest obj)
        {
            if(obj.BigBallFacade == _target) 
                ResetTarget();
        }

        private void OnMoveToBigBall(GameEvents.MoveAllSmallBallsToBigBallSignal obj)
        {
            StartMoveSmallBallToBigBall(obj.BigBallFacade);
        }

        private void StartMoveSmallBallToBigBall(BigBallFacade bigBallFacade)
        {
            _target = bigBallFacade;
        }

        public void FixedTick()
        {
            if (!_target) return;
            
            var dir = (_target.CurrentPosition - _smallBallRigidbodyHandler.CurrentPosition).normalized;
            
            _smallBallRigidbodyHandler.Rigidbody2d.MovePosition(_smallBallRigidbodyHandler.CurrentPosition +
                                                                dir * _settings.moveSpeedToBigBall * Time.fixedDeltaTime);
        }

        public void ResetTarget()
        {
            _target = null;
        }

        [Serializable]
        public class Settings
        {
            [Range(0, 500)] public float moveSpeedToBigBall = 0.5f;
        }
    }
}