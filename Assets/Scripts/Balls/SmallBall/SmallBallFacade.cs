using System;
using Balls.BigBall.ScriptableObject;
using UnityEngine;
using Zenject;

namespace Balls.SmallBall
{
    public class SmallBallFacade : MonoBehaviour, IPoolable<BigBallSettingsSO, IMemoryPool>, IDisposable
    {
        public BigBallSettingsSO CurrentBigBallSettingsSO { get; private set; }

        private IMemoryPool _pool;
        private SmallBallVelocityController _smallBallVelocityController;
        private SmallBallColliderHandler _smallBallColliderHandler;
        private SmallBallAnimationHandler _smallBallAnimationHandler;
        private SmallBallRegistry _smallBallRegistry;
        private SmallBallDestroyHandler _smallBallDestroyHandler;

        [Inject]
        public void Construct(
            SmallBallVelocityController smallBallVelocityController,
            SmallBallColliderHandler smallBallColliderHandler,
            SmallBallAnimationHandler smallBallAnimationHandler,
            SmallBallRegistry smallBallRegistry,
            SmallBallDestroyHandler smallBallDestroyHandler)
        {
            _smallBallVelocityController = smallBallVelocityController;
            _smallBallColliderHandler = smallBallColliderHandler;
            _smallBallAnimationHandler = smallBallAnimationHandler;
            _smallBallRegistry = smallBallRegistry;
            _smallBallDestroyHandler = smallBallDestroyHandler;
        }

        public Vector2 CurrentPosition
        {
            get => transform.position;
            set => transform.position = value;
        }

        public void OnDespawned()
        {
            _smallBallRegistry.RemoveBall(this);

            CurrentBigBallSettingsSO = null;
            _pool = null;

            _smallBallAnimationHandler.ResetTarget();
        }

        public void OnSpawned(BigBallSettingsSO bigBallSettingsSO, IMemoryPool pool)
        {
            CurrentBigBallSettingsSO = bigBallSettingsSO;
            _pool = pool;
            
            _smallBallColliderHandler.SetColliderOnSpawn();
            
            SetBallColor();
            
            _smallBallRegistry.AddBall(this);
        }

        private void SetBallColor()
        {
            GetComponent<SpriteRenderer>().color = CurrentBigBallSettingsSO.GetColor();
        }

        public void SetVelocityOnSpawn()
        {
            _smallBallVelocityController.SetVelocityAtStart();
        }

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        public void DestroyBall()
        {
            _smallBallDestroyHandler.DestroyBall();
        }

        public class Factory : PlaceholderFactory<BigBallSettingsSO, SmallBallFacade> { }
    }
}
