using System;
using Balls.BigBall.ScriptableObject;
using Misc;
using Stats;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.BigBall
{
    public class UIBigBallShopButtonHandler : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI ballNameText;
        [SerializeField] private TextMeshProUGUI upgradeCostText;
        [SerializeField] private TextMeshProUGUI currentUpgradeLevelText;
        [SerializeField] private TextMeshProUGUI currentSmallBallPointMultiplierText;
        [SerializeField] private Image ballImage;

        private BigBallSettingsSO _currentBigBallSettings;
        private GamePointsManager _gamePointsManager;
        private SignalBus _signalBus;

        [Inject]
        public void Construct(GamePointsManager gamePointsManager,
            SignalBus signalBus)
        {
            _gamePointsManager = gamePointsManager;
            _signalBus = signalBus;
        }

        private void OnEnable()
        {
            _signalBus.Subscribe<GameEvents.PointsAdded>(OnPointsAdded);
        }

        private void OnDisable()
        {
            _signalBus.Unsubscribe<GameEvents.PointsAdded>(OnPointsAdded);
        }

        private void OnPointsAdded(GameEvents.PointsAdded obj)
        {
            SetButtonInteractable();
        }

        private void SetButtonInteractable()
        {
            GetComponent<Button>().interactable =
                _gamePointsManager.GetCurrentPoints() >= _currentBigBallSettings.GetUnlockPrice();
        }

        public void SetOnSpawn(BigBallSettingsSO bigBallSetting)
        {
            _currentBigBallSettings = bigBallSetting;

            SetBallNameText();

            SetBallImageColor();
            
            SetUpgradeCostText();

            SetCurrentUpgradeLevelText();

            SetCurrentSmallBallPointMultiplierText();
        }

        private void SetBallNameText()
        {
            ballNameText.SetText(_currentBigBallSettings.GetBallName());
        }

        private void SetBallImageColor()
        {
            ballImage.color = _currentBigBallSettings.GetColor();
        }

        private void SetUpgradeCostText()
        {
            upgradeCostText.SetText(_currentBigBallSettings.GetUnlockPrice().ToString());
        }

        private void SetCurrentUpgradeLevelText()
        {
            currentUpgradeLevelText.SetText(_currentBigBallSettings.CurrentUpgradeLevel.ToString());
        }

        private void SetCurrentSmallBallPointMultiplierText()
        {
            currentSmallBallPointMultiplierText.SetText(_currentBigBallSettings.GetBallPoint().ToString());
        }

        public void UpgradeBigBallOnButtonClick()
        {
            _gamePointsManager.DecreasePoints(_currentBigBallSettings.GetUnlockPrice());

            _currentBigBallSettings.CurrentUpgradeLevel++;
            
            SetUpgradeCostText();
            
            SetCurrentUpgradeLevelText();
            
            SetCurrentSmallBallPointMultiplierText();
            
            SetButtonInteractable();
        }
        
        public class UIBigBallShopButtonHandlerFactory : PlaceholderFactory<UIBigBallShopButtonHandler>
        {
        }
    }
}
