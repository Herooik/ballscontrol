using Balls.BigBall.ScriptableObject;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.BigBall
{
    public class UIBigBallHandler : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI currentPointsText;
        
        public void SetOnSpawn(BigBallSettingsSO bigBallSettingsSO)
        {
            image.color = bigBallSettingsSO.GetColor();
        }

        public void SetCurrentPointsText(int currentPoints)
        {
            currentPointsText.SetText(currentPoints.ToString());
        }
    }
}
