using Balls.BigBall;
using UnityEngine;
using Zenject;

namespace UI.BigBall
{
    public class UIBigBallShopManager : MonoBehaviour
    {
        private UIBigBallShopButtonHandler.UIBigBallShopButtonHandlerFactory _uiBigBallShopButtonHandlerFactory;
        private BigBallSpawnerManager.Settings _bigBallSpawnerManagerSettings;

        [Inject]
        public void Construct(
            UIBigBallShopButtonHandler.UIBigBallShopButtonHandlerFactory uiBigBallShopButtonHandlerFactory,
            BigBallSpawnerManager.Settings bigBallSpawnerManager)
        {
            _uiBigBallShopButtonHandlerFactory = uiBigBallShopButtonHandlerFactory;
            _bigBallSpawnerManagerSettings = bigBallSpawnerManager;
        }

        private void Start()
        {
            SpawnAllButtons();
        }

        private void SpawnAllButtons()
        {
            var settings = _bigBallSpawnerManagerSettings.bigBallSettingsManagerSO;
            
            foreach (var bigBallSetting in settings.GetAllBalls())
            {
                var button = _uiBigBallShopButtonHandlerFactory.Create();

                button.transform.SetParent(transform);
                button.transform.localScale = Vector3.one;

                button.SetOnSpawn(bigBallSetting);
            }
        }
    }
}
