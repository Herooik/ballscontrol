﻿using System;
using Installers;
using Misc;
using Stats;
using TMPro;
using UnityEngine;
using Zenject;

namespace UI
{
    public class UIPointsHandler : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI pointsText;
        [SerializeField] private TextMeshProUGUI currentLevelText;
        
        private SignalBus _signalBus;

        [Inject]
        public void Construct(
            SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        private void OnEnable()
        {
            _signalBus.Subscribe<GameEvents.PointsAdded>(OnPointsAdded);
        }

        private void OnDisable()
        {
            _signalBus.Unsubscribe<GameEvents.PointsAdded>(OnPointsAdded);
        }

        private void OnPointsAdded(GameEvents.PointsAdded obj)
        {
            SetPointsText(obj.CurrentPoints);
        }

        private void SetPointsText(int currentPoints)
        {
            pointsText.SetText(currentPoints.ToString());
        }
    }
}