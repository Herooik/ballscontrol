using System.Collections.Generic;
using System.Linq;
using Camera;
using Cysharp.Threading.Tasks;
using Installers;
using UnityEngine;
using Zenject;

public class UIGameplayAreaWorldPositionGetterHandler : MonoBehaviour
{
    private CameraController _cameraController;

    [Inject]
    public void Construct(CameraController cameraController)
    {
        _cameraController = cameraController;
    }

    private async UniTask<List<Vector2>> SetAndPrepareAsync()
    {
        await UniTask.WaitForEndOfFrame(this);
        
        var vectors = new Vector3[4];
        
        GetComponent<RectTransform>().GetWorldCorners(vectors);

        return vectors.Select(vector => _cameraController.Camera.ScreenToWorldPoint(vector)).Select(dummy => (Vector2)dummy).ToList();
    }

    public async UniTask<Vector2> GetBottomLeftCornerAsync()
    {
        var corners = await SetAndPrepareAsync();
        
        return corners[0];
    }

    public async UniTask<Vector2> GetUpperLeftCornerAsync()
    {
        var corners = await SetAndPrepareAsync();
        
        return corners[1];
    }

    public async UniTask<Vector2> GetUpperRightCornerAsync()
    {
        var corners = await SetAndPrepareAsync();
        
        return corners[2];
    }

    public async UniTask<Vector2> GetBottomRightCornerAsync()
    {
        var corners = await SetAndPrepareAsync();
        
        return corners[3];
    }
}
