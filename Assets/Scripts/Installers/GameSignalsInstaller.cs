using Misc;
using Zenject;

namespace Installers
{
    public class GameSignalsInstaller : Installer<GameSignalsInstaller>
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            
            Container.DeclareSignal<GameEvents.BigBallDestroyedSignalTest>();
            Container.DeclareSignal<GameEvents.MoveAllSmallBallsToBigBallSignal>();
            Container.DeclareSignal<GameEvents.PointsAdded>();
        }
    }
}