using System;
using Balls.BigBall;
using Balls.SmallBall;
using Balls.SmallBall.Spawner;
using Balls.SpawnPoint;
using Balls.SpawnPoint.Spawner;
using Camera;
using UnityEngine;
using Zenject;

namespace Installers
{
    [CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "BallsControl/Game Settings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        [SerializeField] private GameInstaller.Settings gameInstallerSettings;
        [SerializeField] private SmallBallSettings smallBallSettings;
        [SerializeField] private BigBallSpawnPointSettings bigBallSpawnPointSettings;
        [SerializeField] private BigBallSettings bigBallSettings;
        [SerializeField] private CameraSettings cameraSettings;
        
        [Serializable]
        public class SmallBallSettings
        {
            public SmallBallSpawnerManager.Settings spawner;
            public SmallBallVelocityController.Settings velocitySettings;
            public SmallBallAnimationHandler.Settings animationSettings;
        }

        [Serializable]
        public class BigBallSpawnPointSettings
        {
            public BigBallSpawnPointSpawnerManager.Settings spawner;
        }

        [Serializable]
        public class BigBallSettings
        {
            public BigBallSpawnerManager.Settings spawner;
        }
        
        [Serializable]
        public class CameraSettings
        {
            public CameraController.Settings camera;
        }

        public override void InstallBindings()
        {
            Container.BindInstance(gameInstallerSettings);
            
            InstallSmallBallSettings();

            InstallBigBallSpawnPointSettings();

            Container.BindInstance(bigBallSettings.spawner);
            
            Container.BindInstance(cameraSettings.camera);
        }

        private void InstallSmallBallSettings()
        {
            Container.BindInstance(smallBallSettings.spawner);
            Container.BindInstance(smallBallSettings.velocitySettings);
            Container.BindInstance(smallBallSettings.animationSettings);
        }

        private void InstallBigBallSpawnPointSettings()
        {
            Container.BindInstance(bigBallSpawnPointSettings.spawner);
        }
    }

}