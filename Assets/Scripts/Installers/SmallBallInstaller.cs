using Balls.BigBall.ScriptableObject;
using Balls.SmallBall;
using Zenject;

namespace Installers
{
    public class SmallBallInstaller : Installer<SmallBallInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<SmallBallVelocityController>().AsSingle();
            Container.BindInterfacesAndSelfTo<SmallBallDestroyHandler>().AsSingle();
            Container.BindInterfacesAndSelfTo<SmallBallAnimationHandler>().AsSingle();
        }
        
        public class SmallBallFacadePool : MonoPoolableMemoryPool<BigBallSettingsSO,
            IMemoryPool, SmallBallFacade>
        {
        }
    }
}