using System;
using Balls.BigBall;
using Balls.BigBall.ScriptableObject;
using Balls.SmallBall;
using Balls.SmallBall.Spawner;
using Balls.SpawnPoint;
using Balls.SpawnPoint.Spawner;
using Camera;
using Misc;
using Stats;
using UI.BigBall;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class GameInstaller : MonoInstaller
    {
        [Inject] private Settings _settings;

        public override void InstallBindings()
        {
            InstallBallsSpawners();

            InstallFactories();

            InstallMisc();

            GameSignalsInstaller.Install(Container);
            
            InstallExecutionOrder();
        }

        private void InstallBallsSpawners()
        {
            Container.BindInterfacesAndSelfTo<SmallBallSpawnerManager>().AsSingle();

            Container.BindInterfacesAndSelfTo<BigBallSpawnPointSpawnerManager>().AsSingle();

            Container.BindInterfacesAndSelfTo<BigBallSpawnerManager>().AsSingle();
        }

        private void InstallFactories()
        {
            Container.BindFactory<BigBallSettingsSO, SmallBallFacade, SmallBallFacade.Factory>()
                .FromPoolableMemoryPool<BigBallSettingsSO, SmallBallFacade,
                    SmallBallInstaller.SmallBallFacadePool>(poolBinder => poolBinder
                    .WithInitialSize(10)
                    .FromSubContainerResolve()
                    .ByNewPrefabInstaller<SmallBallInstaller>(_settings.smallBallPrefab)
                    .UnderTransformGroup("SmallBalls"));

            Container.BindFactory<BigBallSettingsSO, BigBallSpawnPointFacade, BigBallFacade, BigBallFacade.Factory>()
                .FromPoolableMemoryPool<BigBallSettingsSO, BigBallSpawnPointFacade, BigBallFacade,
                    BigBallInstaller.BigBallFacadePool>(poolBinder => poolBinder
                    .WithInitialSize(10)
                    .FromSubContainerResolve()
                    .ByNewPrefabInstaller<BigBallInstaller>(_settings.bigBallPrefab)
                    .UnderTransformGroup("BigBalls"));
            
            Container.BindFactory<BigBallSpawnPointFacade, BigBallSpawnPointFacade.Factory>()
                .FromComponentInNewPrefab(_settings.bigBallSpawnPointPrefab)
                .WithGameObjectName("BigBallSpawnPoint")
                .UnderTransformGroup("BigBallSpawnPoints");

            Container.BindFactory<UIBigBallShopButtonHandler,
                    UIBigBallShopButtonHandler.UIBigBallShopButtonHandlerFactory>()
                .FromComponentInNewPrefab(_settings.bigBallShopButtonPrefab);
        }

        private void InstallMisc()
        {
            Container.BindInterfacesAndSelfTo<CameraController>().AsSingle();

            Container.BindInterfacesAndSelfTo<GameInputController>().AsSingle();

            Container.Bind<SmallBallRegistry>().AsSingle();

            Container.Bind<BigBallRegistry>().AsSingle();

            Container.BindInterfacesAndSelfTo<GamePointsManager>().AsSingle();
        }

        private void InstallExecutionOrder()
        {
            Container.BindExecutionOrder<BigBallSpawnPointSpawnerManager>(-30);
            Container.BindExecutionOrder<BigBallSpawnerManager>(-20);
            Container.BindExecutionOrder<SmallBallSpawnerManager>(-10);
        }

        [Serializable]
        public class Settings
        {
            public GameObject smallBallPrefab;
            public GameObject bigBallSpawnPointPrefab;
            public GameObject bigBallPrefab;
            public GameObject bigBallShopButtonPrefab;
        }
    }
}

    
    