using Balls.BigBall;
using Balls.BigBall.ScriptableObject;
using Balls.SpawnPoint;
using Zenject;

namespace Installers
{
    public class BigBallInstaller : Installer<BigBallInstaller>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<BigBallPointsController>().AsSingle();
            Container.BindInterfacesAndSelfTo<BigBallDestroyHandler>().AsSingle();
        }

        public class BigBallFacadePool : MonoPoolableMemoryPool<BigBallSettingsSO,
            BigBallSpawnPointFacade, IMemoryPool, BigBallFacade>
        {
        }
    }
}