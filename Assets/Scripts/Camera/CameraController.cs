﻿using System;
using UnityEngine;
using Zenject;

namespace Camera
{
    public class CameraController : ITickable, IInitializable
    {
        public UnityEngine.Camera Camera { get; } = UnityEngine.Camera.main;

        private readonly Settings _settings;
        private Vector3 _upperLeft;
        private Vector3 _upperRight;
        private Vector3 _lowerLeft;
        private Vector3 _lowerRight;

        public CameraController(Settings settings)
        {
            _settings = settings;
        }

        public void Initialize()
        {
            SetCameraOrthographicSize();

            SetCameraCorners();
        }

        private void SetCameraOrthographicSize()
        {
            var height = Mathf.RoundToInt(_settings.targetWidth / (float) Screen.width * Screen.height);
            Camera.orthographicSize = height / 100f / 2f;
        }

        private void SetCameraCorners()
        {
            var upperLeftScreen = new Vector3(0, Screen.height);
            var upperRightScreen = new Vector3(Screen.width, Screen.height);
            var lowerLeftScreen = new Vector3(0, 0);
            var lowerRightScreen = new Vector3(Screen.width, 0);

            _upperLeft = Camera.ScreenToWorldPoint(upperLeftScreen);
            _upperRight = Camera.ScreenToWorldPoint(upperRightScreen);
            _lowerLeft = Camera.ScreenToWorldPoint(lowerLeftScreen);
            _lowerRight = Camera.ScreenToWorldPoint(lowerRightScreen);
        }

        public void Tick()
        {
            // Just for testing dynamic device change in editor
#if UNITY_EDITOR
            SetCameraOrthographicSize();
#endif
        }

        public Vector3 GetUpperLeftCorner()
        {
            return _upperLeft;
        }

        public Vector3 GetUpperRightCorner()
        {
            return _upperRight;
        }
        
        
        [Serializable]
        public class Settings
        {
            public int targetWidth;
        }
    }
}