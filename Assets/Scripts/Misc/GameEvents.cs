using Balls.BigBall;

namespace Misc
{
    public class GameEvents
    {
        public class BigBallDestroyedSignalTest
        {
            public BigBallFacade BigBallFacade { get; }

            public BigBallDestroyedSignalTest(BigBallFacade bigBallFacade)
            {
                BigBallFacade = bigBallFacade;
            }
        }

        public class MoveAllSmallBallsToBigBallSignal
        {
            public BigBallFacade BigBallFacade { get; }
            
            public MoveAllSmallBallsToBigBallSignal(BigBallFacade bigBallFacade)
            {
                BigBallFacade = bigBallFacade;
            }
        }

        public class PointsAdded
        {
            public int CurrentPoints { get; }

            public PointsAdded(int currentPoints)
            {
                CurrentPoints = currentPoints;
            }
        }
    }
}