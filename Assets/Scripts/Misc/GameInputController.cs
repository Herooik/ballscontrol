using System;
using Balls;
using Camera;
using UnityEngine;
using Zenject;

namespace Misc
{
    public class GameInputController : ITickable
    {
        private readonly CameraController _cameraController;
        
        public GameInputController(CameraController cameraController)
        {
            _cameraController = cameraController;
        }
        
        public void Tick()
        {
            if (Input.touchCount <= 0) return;
        
            var touch = GetTouch();
                
            var touchPos = GetTouchPosition(touch);

            var hit = GetRaycastHit(touchPos); 
            
            CheckHit(hit, touch);
        }

        private static Touch GetTouch()
        {
            var touch = Input.GetTouch(0);
            return touch;
        }

        private Vector2 GetTouchPosition(Touch touch)
        {
            var touchPosWorld = _cameraController.Camera.ScreenToWorldPoint(touch.position);

            var touchPos = new Vector2(touchPosWorld.x, touchPosWorld.y);
            return touchPos;
        }

        private RaycastHit2D GetRaycastHit(Vector2 touchPos)
        {
            var hit = Physics2D.Raycast(touchPos, _cameraController.Camera.transform.forward);
            return hit;
        }

        private static void CheckHit(RaycastHit2D hit, Touch touch)
        {
            if (hit.collider?.GetComponent<IInteractableObject>() == null) return;
        
            var interactableObject = hit.collider.GetComponent<IInteractableObject>();

            OnHit(touch, interactableObject);
        }

        private static void OnHit(Touch touch, IInteractableObject interactableObject)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    interactableObject.OnBeganInput();
                    break;
                case TouchPhase.Moved:
                    break;
                case TouchPhase.Stationary:
                    interactableObject.OnStationaryInput();
                    break;
                case TouchPhase.Ended:
                    interactableObject.OnEndedInput();
                    break;
                case TouchPhase.Canceled:

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}