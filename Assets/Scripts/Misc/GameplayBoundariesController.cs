using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Misc
{
    public class GameplayBoundariesController : MonoBehaviour
    {
        private UIGameplayAreaWorldPositionGetterHandler _uiGameplayAreaWorldPositionGetterHandler;

        [Inject]
        public void Construct(UIGameplayAreaWorldPositionGetterHandler uiGameplayAreaWorldPositionGetterHandler)
        {
            _uiGameplayAreaWorldPositionGetterHandler = uiGameplayAreaWorldPositionGetterHandler;
        }
    
        public void Start()
        {
            SetEdgeColliderPointsAsync();
        }

        private async void SetEdgeColliderPointsAsync()
        {
            var pointsList = await SetPointsListAsync();

            GetComponent<EdgeCollider2D>().SetPoints(pointsList);
        }

        private async UniTask<List<Vector2>> SetPointsListAsync()
        {
            var bottomLeftCornerAsync = await _uiGameplayAreaWorldPositionGetterHandler.GetBottomLeftCornerAsync();
            var upperLeftCornerAsync = await _uiGameplayAreaWorldPositionGetterHandler.GetUpperLeftCornerAsync();
            var upperRightCornerAsync = await _uiGameplayAreaWorldPositionGetterHandler.GetUpperRightCornerAsync();
            var bottomRightCornerAsync = await _uiGameplayAreaWorldPositionGetterHandler.GetBottomRightCornerAsync();

            var pointsList = new List<Vector2>
            {
                bottomLeftCornerAsync,
                upperLeftCornerAsync,
                upperRightCornerAsync,
                bottomRightCornerAsync,
                bottomLeftCornerAsync,
            };
            return pointsList;
        }

        private void Update()
        {
            // Just for testing dynamic device change in editor
#if UNITY_EDITOR
            SetEdgeColliderPointsAsync();
#endif
        }
    }
}
