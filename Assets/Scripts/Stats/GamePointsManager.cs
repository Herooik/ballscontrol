﻿using Misc;
using Zenject;

namespace Stats
{
    public class GamePointsManager : IInitializable
    {
        private int _currentPoints;
        private int _currentLevel = 1;
        private readonly SignalBus _signalBus;

        public GamePointsManager(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            FirePointsAddedSignal();
        }

        private void FirePointsAddedSignal()
        {
            _signalBus.Fire(new GameEvents.PointsAdded(_currentPoints));
        }

        public void AddPoint(int pointToAdd)
        {
            _currentPoints += pointToAdd;
            
            FirePointsAddedSignal();
        }

        public void DecreasePoints(int pointsToDecrease)
        {
            _currentPoints -= pointsToDecrease;
            
            FirePointsAddedSignal();
        }

        public int GetCurrentPoints()
        {
            return _currentPoints;
        }
        
        public void AddLevel()
        {
            _currentLevel++;
        }

        public int GetCurrentLevel()
        {
            return _currentLevel;
        }
    }
}